﻿using QuickLogin.ExtControl;
using System.Windows.Forms;

namespace QuickLogin
{
    partial class frmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.lbVersion = new System.Windows.Forms.Label();
            this.btnMin = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnLoginX64 = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.cbxShowPassWord = new System.Windows.Forms.CheckBox();
            this.cbxRemember = new System.Windows.Forms.CheckBox();
            this.lbSev = new System.Windows.Forms.Label();
            this.lbPWD = new System.Windows.Forms.Label();
            this.lbID = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.cblUsername = new System.Windows.Forms.ComboBox();
            this.cblServerList = new System.Windows.Forms.ComboBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtNews = new QuickLogin.RichTextBoxEx();
            this.pnlLink = new System.Windows.Forms.FlowLayoutPanel();
            this.txtInfo = new QuickLogin.ExtControl.AlphaBlendTextBox();
            this.SuspendLayout();
            // 
            // lbVersion
            // 
            this.lbVersion.AutoSize = true;
            this.lbVersion.BackColor = System.Drawing.Color.Transparent;
            this.lbVersion.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lbVersion.ForeColor = System.Drawing.Color.White;
            this.lbVersion.Location = new System.Drawing.Point(74, 456);
            this.lbVersion.Name = "lbVersion";
            this.lbVersion.Size = new System.Drawing.Size(0, 17);
            this.lbVersion.TabIndex = 1;
            // 
            // btnMin
            // 
            this.btnMin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMin.Image = global::QuickLogin.Properties.Resources.M;
            this.btnMin.Location = new System.Drawing.Point(690, 17);
            this.btnMin.Name = "btnMin";
            this.btnMin.Size = new System.Drawing.Size(24, 24);
            this.btnMin.TabIndex = 7;
            this.btnMin.UseVisualStyleBackColor = true;
            this.btnMin.Click += new System.EventHandler(this.btnMin_Click);
            // 
            // btnClose
            // 
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Image = global::QuickLogin.Properties.Resources.X;
            this.btnClose.Location = new System.Drawing.Point(719, 17);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(24, 24);
            this.btnClose.TabIndex = 8;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnLoginX64
            // 
            this.btnLoginX64.Enabled = false;
            this.btnLoginX64.Font = new System.Drawing.Font("Microsoft YaHei UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnLoginX64.Location = new System.Drawing.Point(624, 231);
            this.btnLoginX64.Name = "btnLoginX64";
            this.btnLoginX64.Size = new System.Drawing.Size(83, 34);
            this.btnLoginX64.TabIndex = 28;
            this.btnLoginX64.Text = "64位登陆";
            this.btnLoginX64.UseVisualStyleBackColor = true;
            this.btnLoginX64.Click += new System.EventHandler(this.btnLoginX64_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft YaHei UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnUpdate.Location = new System.Drawing.Point(448, 231);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(83, 34);
            this.btnUpdate.TabIndex = 27;
            this.btnUpdate.Text = "更新";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.BtnUpdate_Click);
            // 
            // cbxShowPassWord
            // 
            this.cbxShowPassWord.AutoSize = true;
            this.cbxShowPassWord.BackColor = System.Drawing.Color.Transparent;
            this.cbxShowPassWord.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbxShowPassWord.Enabled = false;
            this.cbxShowPassWord.Font = new System.Drawing.Font("Microsoft YaHei UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cbxShowPassWord.ForeColor = System.Drawing.Color.White;
            this.cbxShowPassWord.Location = new System.Drawing.Point(624, 212);
            this.cbxShowPassWord.Name = "cbxShowPassWord";
            this.cbxShowPassWord.Size = new System.Drawing.Size(75, 20);
            this.cbxShowPassWord.TabIndex = 26;
            this.cbxShowPassWord.TabStop = false;
            this.cbxShowPassWord.Text = "显示密码?";
            this.cbxShowPassWord.UseVisualStyleBackColor = false;
            this.cbxShowPassWord.CheckedChanged += new System.EventHandler(this.CbxShowPassWord_CheckedChanged);
            // 
            // cbxRemember
            // 
            this.cbxRemember.AutoSize = true;
            this.cbxRemember.BackColor = System.Drawing.Color.Transparent;
            this.cbxRemember.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbxRemember.Checked = true;
            this.cbxRemember.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxRemember.Enabled = false;
            this.cbxRemember.Font = new System.Drawing.Font("Microsoft YaHei UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cbxRemember.ForeColor = System.Drawing.Color.White;
            this.cbxRemember.Location = new System.Drawing.Point(648, 166);
            this.cbxRemember.Name = "cbxRemember";
            this.cbxRemember.Size = new System.Drawing.Size(53, 20);
            this.cbxRemember.TabIndex = 20;
            this.cbxRemember.TabStop = false;
            this.cbxRemember.Text = "记住?";
            this.cbxRemember.UseVisualStyleBackColor = false;
            // 
            // lbSev
            // 
            this.lbSev.AutoSize = true;
            this.lbSev.BackColor = System.Drawing.Color.Transparent;
            this.lbSev.Font = new System.Drawing.Font("Microsoft YaHei UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lbSev.ForeColor = System.Drawing.Color.White;
            this.lbSev.Location = new System.Drawing.Point(490, 98);
            this.lbSev.Name = "lbSev";
            this.lbSev.Size = new System.Drawing.Size(54, 20);
            this.lbSev.TabIndex = 25;
            this.lbSev.Text = "服务器";
            this.lbSev.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbPWD
            // 
            this.lbPWD.AutoSize = true;
            this.lbPWD.BackColor = System.Drawing.Color.Transparent;
            this.lbPWD.Font = new System.Drawing.Font("Microsoft YaHei UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lbPWD.ForeColor = System.Drawing.Color.White;
            this.lbPWD.Location = new System.Drawing.Point(497, 186);
            this.lbPWD.Name = "lbPWD";
            this.lbPWD.Size = new System.Drawing.Size(47, 20);
            this.lbPWD.TabIndex = 24;
            this.lbPWD.Text = "密  码";
            this.lbPWD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbID
            // 
            this.lbID.AutoSize = true;
            this.lbID.BackColor = System.Drawing.Color.Transparent;
            this.lbID.Font = new System.Drawing.Font("Microsoft YaHei UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lbID.ForeColor = System.Drawing.Color.White;
            this.lbID.Location = new System.Drawing.Point(490, 141);
            this.lbID.Name = "lbID";
            this.lbID.Size = new System.Drawing.Size(54, 20);
            this.lbID.TabIndex = 23;
            this.lbID.Text = "用户名";
            this.lbID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnLogin
            // 
            this.btnLogin.Enabled = false;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft YaHei UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnLogin.Location = new System.Drawing.Point(535, 231);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(83, 34);
            this.btnLogin.TabIndex = 22;
            this.btnLogin.Text = "32位登陆";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // cblUsername
            // 
            this.cblUsername.Enabled = false;
            this.cblUsername.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cblUsername.Font = new System.Drawing.Font("Microsoft YaHei UI", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cblUsername.FormattingEnabled = true;
            this.cblUsername.Location = new System.Drawing.Point(550, 139);
            this.cblUsername.Name = "cblUsername";
            this.cblUsername.Size = new System.Drawing.Size(152, 27);
            this.cblUsername.TabIndex = 19;
            this.cblUsername.SelectedIndexChanged += new System.EventHandler(this.cblUsername_TextChanged);
            // 
            // cblServerList
            // 
            this.cblServerList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cblServerList.Enabled = false;
            this.cblServerList.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cblServerList.Font = new System.Drawing.Font("Microsoft YaHei UI", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cblServerList.FormattingEnabled = true;
            this.cblServerList.Location = new System.Drawing.Point(550, 96);
            this.cblServerList.Name = "cblServerList";
            this.cblServerList.Size = new System.Drawing.Size(152, 27);
            this.cblServerList.TabIndex = 18;
            // 
            // txtPassword
            // 
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPassword.Enabled = false;
            this.txtPassword.Font = new System.Drawing.Font("Microsoft YaHei UI", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtPassword.Location = new System.Drawing.Point(550, 185);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(152, 24);
            this.txtPassword.TabIndex = 21;
            this.txtPassword.UseSystemPasswordChar = true;
            this.txtPassword.WordWrap = false;
            // 
            // txtNews
            // 
            this.txtNews.BackColor = System.Drawing.Color.Black;
            this.txtNews.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNews.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtNews.ForeColor = System.Drawing.Color.Lime;
            this.txtNews.Location = new System.Drawing.Point(301, 311);
            this.txtNews.Margin = new System.Windows.Forms.Padding(0);
            this.txtNews.Name = "txtNews";
            this.txtNews.ReadOnly = true;
            this.txtNews.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.txtNews.Size = new System.Drawing.Size(416, 215);
            this.txtNews.TabIndex = 11;
            this.txtNews.TabStop = false;
            this.txtNews.Text = "";
            this.txtNews.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.txtInfo_LinkClicked);
            // 
            // pnlLink
            // 
            this.pnlLink.BackColor = System.Drawing.Color.Transparent;
            this.pnlLink.Location = new System.Drawing.Point(251, 17);
            this.pnlLink.Name = "pnlLink";
            this.pnlLink.Size = new System.Drawing.Size(404, 73);
            this.pnlLink.TabIndex = 29;
            // 
            // txtInfo
            // 
            this.txtInfo.BackAlpha = 10;
            this.txtInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtInfo.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtInfo.ForeColor = System.Drawing.Color.LightGreen;
            this.txtInfo.Location = new System.Drawing.Point(40, 96);
            this.txtInfo.Multiline = true;
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtInfo.Size = new System.Drawing.Size(258, 357);
            this.txtInfo.TabIndex = 30;
            // 
            // frmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackgroundImage = global::QuickLogin.Properties.Resources.MainBackground;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(756, 542);
            this.ControlBox = false;
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.pnlLink);
            this.Controls.Add(this.btnLoginX64);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.cbxShowPassWord);
            this.Controls.Add(this.cbxRemember);
            this.Controls.Add(this.lbSev);
            this.Controls.Add(this.lbPWD);
            this.Controls.Add(this.lbID);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.cblUsername);
            this.Controls.Add(this.cblServerList);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtNews);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnMin);
            this.Controls.Add(this.lbVersion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "快速登陆器";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MouseDownEvent);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MouseMoveEvent);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.Label lbVersion;
        private System.Windows.Forms.Button btnMin;
        private System.Windows.Forms.Button btnClose;
        private RichTextBoxEx txtNews;
        private Button btnLoginX64;
        private Button btnUpdate;
        private CheckBox cbxShowPassWord;
        private CheckBox cbxRemember;
        private Label lbSev;
        private Label lbPWD;
        private Label lbID;
        private Button btnLogin;
        private ComboBox cblUsername;
        private ComboBox cblServerList;
        private TextBox txtPassword;
        private FlowLayoutPanel pnlLink;
        private AlphaBlendTextBox txtInfo;
    }
}