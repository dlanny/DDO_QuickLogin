using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace QuickLogin.Connect
{
    public static class DaemonThreadExt
    {
        static DaemonThread connectThread = null;
        /// <summary>
        /// 开启后台守护线程，处理服务器状态，新闻获取等事件
        /// </summary>
        /// <param name="CallBak"></param>
        public static void Start(Action<ConnectType, object> CallBak)
        {
            connectThread = new DaemonThread(CallBak);
            //开启后台线程，处理外发请求 
            StartNewThread(connectThread.MainThread);
        }
        /// <summary>
        /// 开启后台线程，登陆用户
        /// </summary>
        /// <param name="user"></param>
        public static void Login(User user)
        {
            StartNewThread(() => user.Login(connectThread._onCallBack));
        }
        public static Thread StartNewThread(ThreadStart threadStart)
        {
            Thread td = new Thread(threadStart);
            td.IsBackground = true;//后台线程,程序关闭自动退出
            td.Start();
            return td;
        }

    }

    public class DaemonThread
    {
        public readonly Action<ConnectType, object> _onCallBack;
        public DaemonThread(Action<ConnectType, object> CallBak)
        {
            _onCallBack = CallBak;
        }


        public void MainThread()
        {
            //读取ServerConfig
            var svrCfg = WebServiceComm.GetServerConfig();
            //读取本地保存的用户名密码
            var userLists = new UserList();
            _onCallBack.Invoke(ConnectType.UserListChange, userLists);
            //拼接上方链接

            var linkDic = new Dictionary<string, string>();
            if (svrCfg.TryGetValue("URL.Support", out string Support))
            {
                linkDic.Add("FAQ", Support);
            }
            if (svrCfg.TryGetValue("URL.Home", out string Home))
            {
                linkDic.Add("主页", Home);
            }
            if (svrCfg.TryGetValue("URL.Community", out string Community))
            {
                linkDic.Add("论坛", Community);
            }
            if (svrCfg.TryGetValue("URL.Account", out string Account))
            {
                linkDic.Add("账户", Account);
            }
            if (svrCfg.TryGetValue("URL.NewAccount", out string NewAccount))
            {
                linkDic.Add("新账户", NewAccount);
            }
            if (svrCfg.TryGetValue("URL.ForgotPassword", out string ForgotPassword))
            {
                linkDic.Add("忘记密码", ForgotPassword);
            }
            _onCallBack.Invoke(ConnectType.GetConfigSuccess, linkDic);

            //开启新线程，获取服务器信息
            DaemonThreadExt.StartNewThread(GetDataCenter);
            //开启新线程，获取新闻信息
            DaemonThreadExt.StartNewThread(GetNews);
        }
        /// <summary>
        /// 调取DDO GLS.DataCenterServer/StatusServer 获取服务器信息
        /// </summary>
        public void GetDataCenter()
        {
            bool isGetDataCenter = false;
            while (!isGetDataCenter)
            {
                try
                {
                    _onCallBack(ConnectType.Message, "正在获取服务器信息..");
                    var _Datacenter = WebServiceComm.GetDatacenter();
                    _onCallBack(ConnectType.GetDataCenterSuccess, _Datacenter.Worlds);
                    isGetDataCenter = true;
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex.ToString());
                }
                if (!isGetDataCenter)
                {
                    _onCallBack(ConnectType.Message, "无法连接到服务器 , 3秒后重试 !");
                    Thread.Sleep(1000);
                    _onCallBack(ConnectType.Message, "无法连接到服务器 , 2秒后重试 !");
                    Thread.Sleep(1000);
                    _onCallBack(ConnectType.Message, "无法连接到服务器 , 1秒后重试 !");
                    Thread.Sleep(1000);
                }
            }
        }
        public void GetNews()
        {
            while (true)
            {
                try
                {
                    _onCallBack(ConnectType.Message, "正在获取新闻信息..");
                    _onCallBack(ConnectType.GetNewsSuccess, new RSSReader(WebServiceComm.NewsFeedUrl).NewsList);
                    return;
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex.ToString());
                }
                _onCallBack(ConnectType.Message, "无法获取新闻 , 5秒后重试 !");
                Thread.Sleep(5000);
            }
        }

    }
    public enum ConnectType
    {
        Message,
        GetDataCenterSuccess,
        GetNewsSuccess,
        GetConfigSuccess,
        LoginSuccess,
        LoginFaild,
        UserListChange
    }


}
