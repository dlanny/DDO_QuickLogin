﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace QuickLogin.Utilities
{
    public static class StringHelper
    {
        public static string Replace(string src, Dictionary<string, string> pairs)
        {
            return Regex.Replace(src, "\\{(.+?)\\}", m =>
            {
                if (pairs.TryGetValue(m.Groups[1].Value, out string res)) return res;
                else return string.Empty;
            });
        }
    }
}
